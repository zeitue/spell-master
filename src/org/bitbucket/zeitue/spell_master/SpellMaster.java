package org.bitbucket.zeitue.spell_master;

import java.util.StringTokenizer;
import org.bitbucket.zeitue.spell_master.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class SpellMaster extends Activity implements OnInitListener {
	private TextToSpeech mTts;
	private static final int MY_DATA_CHECK_CODE = 1000;
	int listnumber = 0;
	int on = 0;
	int nx = 1;
	int limit;
	int Correct;
	int Incorrect;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
		Button repeat = (Button) findViewById(R.id.repeat);
		Button enter = (Button) findViewById(R.id.enter);
		final EditText ET = (EditText) findViewById(R.id.EnterText);
		final String[] words = setUp();
		ET.setText("");
		ET.setOnEditorActionListener(new OnEditorActionListener() {

			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					// your additional processing...
					spell(words[on], words[nx]);
					ET.setText("");
					if (words[on] == "")
						sayWord("press enter to start");
					return true;
				} else {
					return false;
				}
			}

		});
		ET.setOnKeyListener(new View.OnKeyListener() {

			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					switch (keyCode) {
					case KeyEvent.KEYCODE_DPAD_CENTER:
					case KeyEvent.KEYCODE_ENTER:
						spell(words[on], words[nx]);
						ET.setText("");
						if (words[on] == "")
							sayWord("press enter to start");
						return true;
					default:
						break;
					}
				}
				return false;
			}
		});
		enter.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				spell(words[on], words[nx]);
				ET.setText("");
				if (words[on] == "")
					sayWord("press enter to start");
			}
		});
		repeat.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				sayWord("Spell the word " + words[on]);

			}
		});
	}

	public void spell(String word, final String next) {
		final EditText ET = (EditText) findViewById(R.id.EnterText);
		final TextView CorrectTextView = (TextView) findViewById(R.id.correct);
		final TextView IncorrectTextView = (TextView) findViewById(R.id.incorrect);
		if (ET.getText().toString().compareToIgnoreCase(word) == 0) {

			if (word != "") {
				sayWord("Word Correct");
				Correct++;
				CorrectTextView.setText("Correct: " + Correct);
				CorrectTextView.refreshDrawableState();
			}
			if (on < limit - 1)
				on++;
			if (nx < limit - 1)
				nx++;
			if (next == "@")
				endDialog();
			else
				sayWord("Spell the word " + next);
		} else {
			if (word != "") {
				sayWord("Word Incorrect");
				final AlertDialog alertDialog = new AlertDialog.Builder(
						SpellMaster.this).create();
				alertDialog.setTitle("Word Corrections");
				alertDialog.setMessage("Your Spelling: "
						+ ET.getText().toString() + "\nCorrect Spelling: "
						+ word);
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								alertDialog.dismiss();
								if (next == "@") {
									endDialog();
								} else
									sayWord("Spell the word " + next);

							}
						});
				alertDialog.show();
				Incorrect++;
				IncorrectTextView.setText("Incorrect: " + Incorrect);
				IncorrectTextView.refreshDrawableState();
			}
			if (on < limit - 1)
				on++;
			if (nx < limit - 1)
				nx++;

		}
	}

	public void onInit(int status) {
		mTts.speak("Welcome to spell master", TextToSpeech.QUEUE_FLUSH, null);
		mTts.speak("press enter to start", TextToSpeech.QUEUE_ADD, null);

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MY_DATA_CHECK_CODE) {
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				// success, create the TTS instance
				mTts = new TextToSpeech(this, this);
			} else {
				// missing data, install it
				Intent installIntent = new Intent();
				installIntent
						.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installIntent);
			}
		}
	}

	@Override
	public void onDestroy() {
		// Don't forget to shutdown!
		if (mTts != null) {
			mTts.stop();
			mTts.shutdown();
		}
		super.onDestroy();
	}

	public void sayWord(String word) {
		mTts.speak(word, TextToSpeech.QUEUE_ADD, null);
	}

	public String[] setUp() {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		String number = sp.getString("number", "1");
		listnumber = Integer.parseInt(number) - 1;
		String list = sp.getString("list" + (listnumber + 1), 
				"the,what,for,am,to,do,who,how");
		StringTokenizer token = new StringTokenizer(list, ",");
		int num = 0;
		limit = token.countTokens() + 2;
		String[] words = new String[limit];
		words[num] = "";
		num++;
		while (token.hasMoreTokens()) {
			words[num++] = token.nextToken();

		}
		words[num++] = "@";
		return words;
	}

	public void endDialog() {
		sayWord("You have reached the end of your spelling list");
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("End of list");
		alertDialog
				.setMessage("You have reached the end of your spelling list");
		alertDialog.setPositiveButton("Restart",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						Intent intent = getIntent();
						finish();
						startActivity(intent);

					}
				});
		alertDialog.setNegativeButton("Quit",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						int pid = android.os.Process.myPid();
						android.os.Process.killProcess(pid);
					}

				});
		alertDialog.show();
	}

}
