package org.bitbucket.zeitue.spell_master;

import org.bitbucket.zeitue.spell_master.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Start extends Activity {
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start);

		Button Start = (Button) findViewById(R.id.start);
		Start.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent StartIntent = new Intent(Start.this, SpellMaster.class);
				startActivity(StartIntent);
			}
		});

		Button Preferences = (Button) findViewById(R.id.preferences);
		Preferences.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent PreferencesIntent = new Intent(Start.this,
						Preferences.class);
				startActivity(PreferencesIntent);
			}
		});

		Button Help = (Button) findViewById(R.id.help);
		Help.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent HelpIntent = new Intent(Start.this, Help.class);
				startActivity(HelpIntent);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Preferences").setIcon(R.drawable.preferences);
		menu.add(1, 1, 1, "Help").setIcon(R.drawable.help);
		return true;

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			Intent PreferencesIntent = new Intent(Start.this, Preferences.class);
			startActivity(PreferencesIntent);
			return true;
		case 1:
			Intent HelpIntent = new Intent(Start.this, Help.class);
			startActivity(HelpIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void noConnectivity() {
		AlertDialog alertDialog = new AlertDialog.Builder(Start.this).create();
		alertDialog
				.setTitle("This application is free but requires an internet connection");
		alertDialog
				.setMessage("Please configure your connectivity settings and re-try");
		alertDialog.setButton("Exit", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finish();
			}
		});
		alertDialog.show();
	}
}